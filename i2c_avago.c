/*
 * proximity sensor avago ADP-9900
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <time.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>

#ifndef DEV_ID 
# define DEV_ID 0x39
#endif

/*
 * ALS timing register (0x01)
 * 0xff: 1 cycle, 2.72ms time, 1023 max count
 * 0xf6: 10, 27.2 ms, 10239 
 * 0xdb: 37, 100.64ms, 37887
 * 0xc0: 64, 174.08ms, 65535
 * 0x00: 256, 696.32ms, 65535
 */  
#define ATIME 0xf6

#define WTIME 0xff
#define PTIME 0xff
/* pulse count: 0-255 higher value more sensitive */
#define PPCOUNT 5

#define PDRIVE 0
#define PDIODE 0x20
#define PGAIN 0
#define AGAIN 0

enum {
  WEN = 8, /* enable wait */
  PEN = 4, /* enable prox */
  AEN = 2, /* enable als */
  PON = 1 /* power on */
};

static inline
void write_u8 (int dev, unsigned char reg, unsigned char data)
{
  wiringPiI2CWriteReg8 (dev, 0x80|reg, data);
}

static inline
unsigned short read_u16 (int dev, unsigned char reg)
{
  return wiringPiI2CReadReg16 (dev, 0xa0|reg);
}

int
main (int argc, char *argv[])
{
  int dev, val, i;
  unsigned short ch0, ch1, prox;

  if ((dev = wiringPiI2CSetup (DEV_ID)) == -1)
    {
      fprintf (stderr, "error: ** can't initialize device id %d\n", DEV_ID);
      return 1;
    }

  val = wiringPiI2CReadReg8 (dev, 0xa0|0x11);
  fprintf (stderr, "Rev: 0x%x\n", val);
  val = wiringPiI2CReadReg8 (dev, 0xa0|0x12);
  fprintf (stderr, "Dev: %s\n", val == 0x29 ? "APDS-9900" : "APDS-9901");

  /* setup the device */
  write_u8 (dev, 0x0, 0); 
  write_u8 (dev, 0x1, ATIME);
  write_u8 (dev, 0x2, PTIME);
  write_u8 (dev, 0x3, WTIME);
  write_u8 (dev, 0xe, PPCOUNT);
  write_u8 (dev, 0xf, PDRIVE|PDIODE|PGAIN|AGAIN);
  //write_u8 (dev, 0x0, WEN|PEN|AEN|PON);
  write_u8 (dev, 0x0, PEN|AEN|PON);

  delay(12);
  { struct timeval start, end;
    int max = 0, pprox = 0;
    unsigned long w = 0;
    while (1)
      {
	//ch0 = read_u16 (dev, 0x14);
	//ch1 = read_u16 (dev, 0x16);
	prox = read_u16 (dev, 0x18);
	if (prox > 0)
	  {
	    if (pprox == 0)
	      ++w;
	    if (prox >= max)
	      {
		max = prox;
		if (w % 2 == 0)
		  gettimeofday (&end, 0);
		else
		  gettimeofday (&start, 0);
	      }
	  }
	else 
	  { /* reset */
	    if (pprox > 0 && w % 2 == 0)
	      {
		printf ("period %lfs\n",(end.tv_sec - start.tv_sec)
			+ (end.tv_usec-start.tv_usec)/1e6); 
	      }
	    max = 0;
	  }
	pprox = prox;
	//printf ("%d %d %d\n", ch0, ch1, prox);
	//printf ("%d\n", prox);
      }
  }

  return 0;
}
