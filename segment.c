#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>

#include <wiringPi.h>

int
main (int argc, char *argv[])
{
  int pins[] = {
    18, /* a */
    23, /* b */
    24, /* c */
    25, /* d */
    17, /* e */
    27, /* f */
    22  /* g */
  };

  int size =  sizeof (pins)/sizeof (pins[0]);
  int digits[10][sizeof (pins)/sizeof (pins[0])] = {
   /*a, b, c, d, e, f, g */
    {1, 1, 1, 1, 1, 1, 0}, /* 0 */
    {0, 1, 1, 0, 0, 0, 0}, /* 1 */
    {1, 1, 0, 1, 1, 0, 1}, /* 2 */
    {1, 1, 1, 1, 0, 0, 1}, /* 3 */
    {0, 1, 1, 0, 0, 1, 1}, /* 4 */
    {1, 0, 1, 1, 0, 1, 1}, /* 5 */
    {1, 0, 1, 1, 1, 1, 1}, /* 6 */
    {1, 1, 1, 0, 0, 0, 0}, /* 7 */
    {1, 1, 1, 1, 1, 1, 1}, /* 8 */
    {1, 1, 1, 0, 0, 1, 1} /* 9 */
  };
  int i, j;

    /* gpio pin numbering */
  if (wiringPiSetupGpio () == -1)
    {
      fprintf (stderr, "%s: Unable to initialise GPIO mode.\n", argv [0]) ;
      return 1;
    }

  for (i = 0; i < size; ++i)
    {
      pinMode (pins[i], OUTPUT);
      digitalWrite (pins[i], LOW);
    }

  for (i = 0; i < 100; ++i)
    {
      int* d = digits[lrand48()%10];
      for (j = 0; j < size; ++j)
	digitalWrite (pins[j], d[j]);
      delay (500);
    }

  /* clear */
  for (i = 0; i < size; ++i) 
      digitalWrite (pins[i], LOW);

  return 0;
}
