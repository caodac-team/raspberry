/*
 * combining avago & 7-segment to display timer to simulate
 * a photogate system.
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <time.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>

#define DISPLAY_ID 0x71
#define SENSOR_ID 0x39

#define ATIME 0xf6
#define WTIME 0xff
#define PTIME 0xff
/* pulse count: 0-255 higher value more sensitive */
#define PPCOUNT 25

#define PDRIVE 0
#define PDIODE 0x20
#define PGAIN 0
#define AGAIN 0

enum {
  WEN = 8, /* enable wait */
  PEN = 4, /* enable prox */
  AEN = 2, /* enable als */
  PON = 1 /* power on */
};

static int display_dev, sensor_dev;
static char DIGITS[256] = {0};


static inline void digit1 (char b)
{
  wiringPiI2CWriteReg8 (display_dev, 0x7b, b);
}

static inline void digit2 (char b)
{
  wiringPiI2CWriteReg8 (display_dev, 0x7c, b);
}

static inline void digit3 (char b)
{
  wiringPiI2CWriteReg8 (display_dev, 0x7d, b);
}

static inline void digit4 (char b)
{
  wiringPiI2CWriteReg8 (display_dev, 0x7e, b);
}

static inline void sensor_write_u8 (unsigned char reg, unsigned char data)
{
  wiringPiI2CWriteReg8 (sensor_dev, 0x80|reg, data);
}

static inline unsigned short sensor_read_u16 (unsigned char reg)
{
  return wiringPiI2CReadReg16 (sensor_dev, 0xa0|reg);
}

void clear ()
{
  wiringPiI2CWriteReg8 (display_dev, 0x76, 0);
  digit1 (0);
  digit2 (0);
  digit3 (0);
  digit4 (0);
}

void display_int (int value)
{
  int d1 = value / 1000;
  int d2 = (value % 1000) / 100;
  int d3 = (value % 100) / 10;
  int d4 = (value % 10);
  if (d1 == 0) digit1 (0); // clear
  else digit1 (DIGITS['0' + d1]);
  if (d1 == 0 && d2 == 0) digit2 (0);
  else digit2 (DIGITS['0' + d2]);
  if (d1 == 0 && d2 == 0 && d3 == 0) digit3 (0);
  else digit3 (DIGITS['0' + d3]);
  digit4 (DIGITS['0'+d4]);
}

void
display_double (double value)
{
  char str[32] = {0}, *ptr;
  sprintf (str, "%.3lf", value);

  /* clear out any previous display */
  wiringPiI2CWriteReg8 (display_dev, 0x77, 0);

  ptr = strchr (str, '.');
  if (ptr - str > 3)
    sprintf (str, "%.0lf", value);
  else if (ptr - str > 2)
    sprintf (str, "%.1lf", value);
  else if (ptr - str > 1)
    sprintf (str, "%.2lf", value);

  ptr = str;
  digit1 (DIGITS[(int)*ptr++]);
  if (*ptr == '.')
    {
      wiringPiI2CWriteReg8 (display_dev, 0x77, 1);
      digit2 (DIGITS[(int)*++ptr]);
      digit3 (DIGITS[(int)*++ptr]);
      digit4 (DIGITS[(int)*++ptr]);
    }
  else
    {
      digit2 (DIGITS[(int)*ptr++]);
      if (*ptr == '.')
	{
	  wiringPiI2CWriteReg8 (display_dev, 0x77, 2);
	  digit3 (DIGITS[(int)*++ptr]);
	  digit4 (DIGITS[(int)*++ptr]); 
	}
      else
	{
	  digit3 (DIGITS[(int)*ptr++]);
	  if (*ptr == '.')
	    {
	      wiringPiI2CWriteReg8 (display_dev, 0x77, 4);
	      digit4 (DIGITS[(int)*++ptr]);
	    }
	  else
	    {
	      if (ptr[1] == '.')
		wiringPiI2CWriteReg8 (display_dev, 0x77, 8);
	      digit4 (DIGITS[(int)*ptr]); 
	    }
	}
    }
}

static int init ()
{
  /*
   * init devices
   */
  display_dev = wiringPiI2CSetup (DISPLAY_ID);
  if (display_dev == -1)
    {
      fprintf (stderr, "error: ** can't initialize display device at 0x%x\n", 
	       DISPLAY_ID);
      return 1;
    }

  sensor_dev = wiringPiI2CSetup (SENSOR_ID);
  if (sensor_dev == -1)
    {
      fprintf (stderr, "error: ** can't initialize sensor device at 0x%x\n",
	       SENSOR_ID);
      return 1;
    }

  /* 
   * seup display lookup 
   */
  DIGITS['0'] = 63;
  DIGITS['1'] = 6;
  DIGITS['2'] = 91;
  DIGITS['3'] = 79;
  DIGITS['4'] = 102;
  DIGITS['5'] = 109;
  DIGITS['6'] = 125;
  DIGITS['7'] = 39; // 7 or 39
  DIGITS['8'] = 127;
  DIGITS['9'] = 111; // 103 or 111 

  DIGITS['a'] = 95;
  DIGITS['F'] = DIGITS['f'] = 113;
  DIGITS['h'] = 116;
  DIGITS['p'] = DIGITS['P'] = 115;
  DIGITS['y'] = 110;

  // brightness: 0 (dimmed) - 255 (bright) 
  wiringPiI2CWriteReg8 (display_dev, 0x7a, 220);
  /*
   *  baud rate: 2 - 9600 (default), 3 - 14400, 4 - 19200, 5 - 38400
   *  6 - 57600, 7 - 76800, 8 - 115200, 9 - 250000, 10 - 500000, 11 - 1000000
   */
  wiringPiI2CWriteReg8 (display_dev, 0x7f, 6);

  /* 
   * setup the sensor
   */
  sensor_write_u8 (0x0, 0); 
  sensor_write_u8 (0x1, ATIME);
  sensor_write_u8 (0x2, PTIME);
  sensor_write_u8 (0x3, WTIME);
  sensor_write_u8 (0xe, PPCOUNT);
  sensor_write_u8 (0xf, PDRIVE|PDIODE|PGAIN|AGAIN);
  sensor_write_u8 (0x0, WEN|PEN|AEN|PON);
  //sensor_write_u8 (0x0, PEN|AEN|PON);
  delay (12);

  return 0;
}

int
main (int argc, char *argv[])
{
  int err;
  struct timeval start, end;

  err = init ();
  if (err)
    return err;

  clear ();

#ifdef PROX
  { int prox, min = -1, max = -1;
    int prev = 0;
    while (1)
      {
	prox = sensor_read_u16 (0x18);
	
	if (prox > prev)
	  {
	    if (min == prev)
	      {
		double period;
		//time_t t = time (0);
		gettimeofday (&end, 0);
		period = 2*((end.tv_sec - start.tv_sec)
			    + (end.tv_usec-start.tv_usec)/1e6);
		//printf ("<< end time: %s", ctime (&t));
		//printf ("     period: %.3fs\n", period);
		display_double (period);
	      }
	    //printf ("++ %d %d %d\n", prox, prev, min);
	    max = prox;
	  }
	else if (prox < prev)
	  {
	    if (max == prev)
	      {
		//time_t t = time (0);
		gettimeofday (&start, 0);
		//printf (">> start time: %s", ctime (&t));
	      }
	    //printf ("-- %d %d %d\n", prox, prev, max);
	    min = prox;
	  }
#if 0
	if (prox != prev)
	  display_int (prox);
#endif
	prev = prox;
      }
  }
#else
  { int ch0, min = -1, max = -1;
    int prev = 1000;
    while (1)
      {
	ch0 = sensor_read_u16 (0x14); /* ambient light */
	
	if (ch0 == 0)
	  ;
	else if (ch0 < prev)
	  {
	    if (max == prev)
	      {
		double period;
		//time_t t = time (0);
		gettimeofday (&end, 0);
		period = 2*((end.tv_sec - start.tv_sec)
			    + (end.tv_usec-start.tv_usec)/1e6);
		//printf ("<< end time: %s", ctime (&t));
		//printf ("     period: %.3fs\n", period);
		display_double (period);
	      }

	    //printf ("++ %d %d\n", ch0, prev);
	    min = ch0;
	  }
	else if (ch0 > prev)
	  {
	    if (min == prev)
	      {
		//time_t t = time (0);
		gettimeofday (&start, 0);
		//printf (">> start time: %s", ctime (&t));
	      }
	    //printf ("-- %d %d\n", ch0, prev);
	    max = ch0;
	  }
#if 0
	if (ch0 != prev)
	  display_int (ch0);
#endif
	prev = ch0;
      }
  }
#endif

  return 0;
}

/*
 * Local Variables: 
 * compile-command: "gcc -Wall -DPROX -g -o i2c_timer i2c_timer.c -lwiringPi"
 * End:
 */
