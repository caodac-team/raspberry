/*
 * proximity sensor VCNL4000
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>

#ifndef DEV_ID 
# define DEV_ID 0x13
#endif

#define PIN 4

static int lock = 0xdeadbeef;
static int rate = 50;

static void pulse ()
{
  int p = 1;
  while (1)
    {
      digitalWrite (PIN, p);
      delay (rate);
      p = 1-p;
    }
}

int
main (int argc, char *argv[])
{
  int dev, val, i;

  if ((dev = wiringPiI2CSetup (DEV_ID)) == -1)
    {
      fprintf (stderr, "error: ** can't initialize device id %d\n", DEV_ID);
      return 1;
    }

  if (wiringPiSetupGpio () == -1)
    {
      fprintf (stderr, "%s: Unable to initialise GPIO mode.\n", argv [0]) ;
      return 1;
    }

  pinMode (PIN, OUTPUT);
  //piThreadCreate (pulse);

#if 0
  if (fork ())
    pulse ();
#endif

  /* read register #1 and print the product id */
  val = wiringPiI2CReadReg8 (dev, 0x81);
  printf ("## product id: 0x%x%x\n", (val >> 4) & 0x0f, val & 0x0f);

  val = wiringPiI2CReadReg8 (dev, 0x84);
  printf ("## conversion mode=%x offset=%x ave=%x\n",
	  val & 0x80, (val >> 3) & 0x1, val & 0x07);

  /* embient light params */
  wiringPiI2CWriteReg8 (dev, 0x84, 0x8f);

  /* proximity signal freq: 
     0 = 3.125Mhz, 1 = 1.5625Mhz, 2 = 781khz, 3 = 390khz */
  wiringPiI2CWriteReg8 (dev, 0x89, 1);

  /*
   * registers 5 & 6 are ambient light values
   */
  /* measure ambient light & proximity; bits 4 & 3 respectively */

  //for (i = 0; i < 10000; ++i)
  while (1)
    { int status;
      wiringPiI2CWriteReg8 (dev, 0x80, 0x18); 
      //status = wiringPiI2CReadReg8 (dev, 0x80);
      //printf ("alc=%d prox=%d ", status&0x40, status & 0x20);
      //if ((status & 0x20) /*&& (status & 0x40)*/)
	{
	  int hi, lo;
	  hi = wiringPiI2CReadReg8 (dev, 0x85);
	  lo = wiringPiI2CReadReg8 (dev, 0x86);
	  printf ("%d ", ((hi&0xff) << 8) | (lo & 0xff));

	  hi = wiringPiI2CReadReg8 (dev, 0x87);
	  lo = wiringPiI2CReadReg8 (dev, 0x88);
	  val = ((hi&0xff) << 8) | (lo & 0xff);
	  printf ("%d\n", val);
	  //break;
	}
      //delay (10);
    }

  return 0;
}
