/*
 * sparkfun serial 7 segment display
 * References:
 * https://github.com/sparkfun/Serial7SegmentDisplay/wiki/Special-Commands
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <time.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>


/**
Special Command	Command byte	Data byte range	Data byte description
Clear display	0x76	None
Decimal control	0x77	0-63	1 bit per decimal
Cursor control	0x79	0-3	0=left=most, 3=right-most
Brightness control	0x7A	0-255	0=dimmest, 255=brightest
Digit 1 control	0x7B	0-127	1 bit per segment
Digit 2 control	0x7C	0-127	1 bit per segment
Digit 3 control	0x7D	0-127	1 bit per segment
Digit 4 control	0x7E	0-127	1 bit per segment
Baud rate config	0x7F	0-11	See baud section
I2C address Config	0x80	1-126	Data byte is I2C addres
Factory reset	0x81	None
*/

/*
 * the segment layout is as follows where each number denotes the
 * bit position
 *
 *   0000
 *  5    1
 *  5    1
 *   6666
 *  4    2
 *  4    2
 *   3333
 * 
 * So for example, to display number 3, bits 0, 1, 2, 3, and 6 need
 * to be on. This a hex value of 2^0+2^1+2^2+2^3+2^6 = 79 = 0x4f
 */
static char DIGITS[256];

#ifndef DEV_ID 
# define DEV_ID 0x71
#endif

static int DEV;

void
init ()
{
  DIGITS['0'] = 63;
  DIGITS['1'] = 6;
  DIGITS['2'] = 91;
  DIGITS['3'] = 79;
  DIGITS['4'] = 102;
  DIGITS['5'] = 109;
  DIGITS['6'] = 125;
  DIGITS['7'] = 7; // 7 or 39
  DIGITS['8'] = 127;
  DIGITS['9'] = 111; // 103 or 111 

  DIGITS['a'] = 95;
  DIGITS['F'] = DIGITS['f'] = 113;
  DIGITS['h'] = 116;
  DIGITS['p'] = DIGITS['P'] = 115;
  DIGITS['y'] = 110;
}

void
digit1 (char b)
{
  wiringPiI2CWriteReg8 (DEV, 0x7b, b);
}

void
digit2 (char b)
{
  wiringPiI2CWriteReg8 (DEV, 0x7c, b);
}

void
digit3 (char b)
{
  wiringPiI2CWriteReg8 (DEV, 0x7d, b);
}

void
digit4 (char b)
{
  wiringPiI2CWriteReg8 (DEV, 0x7e, b);
}

void
digits (char a, char b, char c, char d)
{
  digit1 (a);
  digit2 (b);
  digit3 (c);
  digit4 (d);
}

void
clear ()
{
  wiringPiI2CWriteReg8 (DEV, 0x76, 0);
  digit1 (0);
  digit2 (0);
  digit3 (0);
  digit4 (0);
}

void
brightness (unsigned char value)
{
  wiringPiI2CWriteReg8 (DEV, 0x7a, value);
}

void
display_int (int value)
{
  int d1 = value / 1000;
  int d2 = (value % 1000) / 100;
  int d3 = (value % 100) / 10;
  int d4 = (value % 10);
  if (d1 == 0) digit1 (0); // clear
  else digit1 (DIGITS['0' + d1]);
  if (d1 == 0 && d2 == 0) digit2 (0);
  else digit2 (DIGITS['0' + d2]);
  if (d1 == 0 && d2 == 0 && d3 == 0) digit3 (0);
  else digit3 (DIGITS['0' + d3]);
  digit4 (DIGITS['0'+d4]);
}

void
display_double (double value)
{
  char str[32] = {0}, *ptr;

  sprintf (str, "%.3lf", value);

  /* clear out any previous display */
  wiringPiI2CWriteReg8 (DEV, 0x77, 0);

  ptr = strchr (str, '.');
  if (ptr - str > 3)
    sprintf (str, "%.0lf", value);
  else if (ptr - str > 2)
    sprintf (str, "%.1lf", value);
  else if (ptr - str > 1)
    sprintf (str, "%.2lf", value);

  ptr = str;
  digit1 (DIGITS[*ptr++]);
  if (*ptr == '.')
    {
      digit2 (DIGITS[*++ptr]);
      digit3 (DIGITS[*++ptr]);
      digit4 (DIGITS[*++ptr]);
      wiringPiI2CWriteReg8 (DEV, 0x77, 1);
    }
  else
    {
      digit2 (DIGITS[*ptr++]);
      if (*ptr == '.')
	{
	  digit3 (DIGITS[*++ptr]);
	  digit4 (DIGITS[*++ptr]); 
	  wiringPiI2CWriteReg8 (DEV, 0x77, 2);
	}
      else
	{
	  digit3 (DIGITS[*ptr++]);
	  if (*ptr == '.')
	    {
	      digit4 (DIGITS[*++ptr]);
	      wiringPiI2CWriteReg8 (DEV, 0x77, 4);
	    }
	  else
	    {
	      digit4 (DIGITS[*ptr]); 
	      if (ptr[1] == '.' || ptr[1] == '\0')
		wiringPiI2CWriteReg8 (DEV, 0x77, 8);
	    }
	}
    }
}

void 
decimal ()
{
  int i;
  for (i = 0; i < 64; ++i)
    {
      wiringPiI2CWriteReg8 (DEV, 0x77, i);
      printf ("configuration %d; hit enter...", i);
      fgetc (stdin);
    }
}

void
counter ()
{
  int i;
  for (i = 0; i < 10000; ++i)
    {
      display_int (i);
      delay (100);
    }
  clear ();
}

void 
segment ()
{
  int i;
  for (i = 0; i < 128; ++i)
    {
      digit4 (i);
      printf ("value %d; hit enter...", i);
      fgetc (stdin);
    }
}

int
main (int argc, char *argv[])
{
  int i;

  if ((DEV = wiringPiI2CSetup (DEV_ID)) == -1)
    {
      fprintf (stderr, "error: ** can't initialize device id %d\n", DEV_ID);
      return 1;
    }

  init ();
  clear ();
  brightness (200);
  //decimal ();

#if 1
  for (i = 1; i < argc; ++i)
    {
      double v = atof (argv[i]);
      display_double (v);
      printf ("display %lf; hit enter...", v);
      fgetc (stdin);
    }
#endif
  //counter ();


  return 0;
}

/*
 * Local Variables: 
 * compile-command: "gcc -Wall -g -o i2c_segment i2c_segment.c -lwiringPi"
 * End:
 */
